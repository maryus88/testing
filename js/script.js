(function($){
	$(document).ready(function(){

		// Accordeon for devices that have a screen size larger than 640px;
		if ($(window).width() > 640) {
			$('#accordeon-items .accordeon-item').hover(
				function(){
					// do logic only for elements that don't have open class. 
					if (!$(this).hasClass('open')) {
						// save default width of element (before sliding).
						var defaultWidth = $(this).width();
						// mark element showing in progress.
						$(this).addClass('in-progress');

						// if hovering on multiple elements, stop animation for the others, 
						// also, remove open class forom all and set width to none. Then, for
						// element in progress, add open class. 
						$('#accordeon-items .accordeon-item').stop(true, true);
						if ($('#accordeon-items .accordeon-item.open').length > 1) {
							$('#accordeon-items .accordeon-item.open').removeClass('open').width('');
							$('#accordeon-items .accordeon-item.in-progress').addClass('open');	
						}

						// define animation
						$(this).stop( true, false ).animate({
							width: '100%' // increase width to maximum.
						}, 
						{
							duration: 400, 
							// function called on each step. 
							progress: function(animation, progress, remainingMs) {
								// compute the width of the element that will have it's width dereased. 
								var width = Math.max(defaultWidth, 730 - $(this).width());
								// if width is more than default width (of collapsed element => ~35px), then set the new width. 
								if (width > defaultWidth) {
									$('#accordeon-items .accordeon-item.open').width(width);
								}
								else { // prevent setting width less then defaut (35px).
									$('#accordeon-items .accordeon-item.open').removeClass('open').width('');
									$(this).addClass('open');
								}
							},
							complete: function() {
								// on complete, also add open class (optional, because in this case, else condition above 
									// will set the class too)
								$(this).addClass('open');
							}

						});
						// $(this).addClass('open');
					}
				}
			);
		}

		move();
		
		// Force scroll on .details blocks
		$('.scroll-x div').css('width', $('.details').width());
		$('.scroll-y div').css('height', $('.details .scaled').height());

		// Tooltip added on links from the first column from footer
		$('.footer-box').first().tooltip({ position: "bottom left"});

		// Custom calculations for devices that have a screen size smaller than 640px;
		if ($(window).width() <= 640) {
			// Menu for mobile devices
			$('.menu-anchor').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				$('#main-menu').slideToggle();
			});

			// Accordeon actions for mobile only
			$('.accordeon-item .bar').click(function(){
				if($(this).parent().hasClass('open')) {
					$(this).siblings().slideUp('fast');
					$(this).parent().removeClass('open');
				}
				else {
					$('.accordeon-item').removeClass('open');
					$('.accordeon-item .acc-detail').slideUp('1');
					$(this).siblings().slideDown('fast');					
					$(this).parent().addClass('open');
				}
			});
		}

	});
	
	$(window).scroll(function(){
		move();
	});

		$(window).resize(function(){
			// Force scroll on .details blocks at rezise event
			$('.scroll-x div').css('width', $('.details').width());
			$('.scroll-y div').css('height', $('.details .scaled').height());
		});

		var move = function() {
			var st = $(window).scrollTop() - $('header').height();
			var ot = $("header").offset().top;
			var s = $("header");

			if(st >= ot || ot) {
				s.css({
					position: "fixed",
					top: 0,
					width: '100%',
					'z-index': 999,
					height: '40px'
				});
				s.addClass('in-action');

			} else {
				if(st < ot) {
					s.css({
							position: "relative",
							top: "",
							height: ''
					});
					s.removeClass('in-action');
				}
			}
		};
	
})(jQuery);